import 'package:first_project_flutter/constants.dart';
import 'package:first_project_flutter/screens/welcome/welcome_screen.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Auth',
        //theme: ThemeData(
        //primaryColor: kPrimaryColor,
        //scaffoldBackgroundColor: Colors.blueGrey,
        //),

        home: WelcomeScreen());
  }
}
