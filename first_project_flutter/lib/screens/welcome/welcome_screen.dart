import 'package:first_project_flutter/screens/welcome/components/body.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Internet Usage Survey"),
          backgroundColor: Colors.deepOrangeAccent),
      body: Body(),
    );
  }
}
