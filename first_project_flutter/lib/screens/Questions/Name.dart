import 'package:first_project_flutter/screens/Questions/one.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart' as validator;
import 'package:firebase_core/firebase_core.dart';

//widget
class NameScreen extends StatefulWidget {
  NameScreen({required this.app});
  final FirebaseApp app;
  @override
  _NameScreenState createState() => _NameScreenState();
}

//state of the widget
class _NameScreenState extends State<NameScreen> {
  final GlobalKey<FormState> formKey = GlobalKey();
  final referenceDatabase = FirebaseDatabase.instance;
  final TextEditingController firstnameController = TextEditingController();
  final TextEditingController lastnameController = TextEditingController();
  String firstName = '';
  String lastName = '';

  Widget buildfirstname() => TextFormField(
        controller: firstnameController,
        decoration: InputDecoration(
            labelText: 'First Name',
            labelStyle: TextStyle(
              color: Colors.black,
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 2, color: Colors.black),
              borderRadius: BorderRadius.circular(15),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 2, color: Colors.black),
              borderRadius: BorderRadius.circular(15),
            ),
            fillColor: Colors.white,
            prefixIcon: Icon(
              Icons.person,
              color: Colors.black,
            )),
        validator: (value) {
          if (value!.length < 4) {
            return 'First Name Required';
          } else {
            return null;
          }
        },
        onSaved: (value) => setState(() => firstName = value!),
        keyboardType: TextInputType.name,
      );
  Widget buildlastname() => TextFormField(
        controller: lastnameController,
        decoration: InputDecoration(
            labelText: 'Last Name',
            labelStyle: TextStyle(
              color: Colors.black,
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 2, color: Colors.black),
              borderRadius: BorderRadius.circular(15),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 2, color: Colors.black),
              borderRadius: BorderRadius.circular(15),
            ),
            prefixIcon: Icon(
              Icons.person,
              color: Colors.black,
            )),
        validator: (value) {
          if (value!.length < 4) {
            return 'Last Name Required';
          } else {
            return null;
          }
        },
        onSaved: (value) => setState(() => lastName = value!),
        keyboardType: TextInputType.name,
      );

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final ref = referenceDatabase.refernce();
    return Scaffold(
        appBar: AppBar(
            title: Text("Internet Usage Survey"),
            backgroundColor: Colors.deepOrangeAccent),
        body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.amberAccent, Colors.redAccent])),
            child: Stack(children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Positioned(
                        child: Image.asset(
                      "assets/images/Name.png",
                      alignment: Alignment.center,
                      height: size.height * 0.18,
                      width: size.width * 0.4,
                    )),
                    //SizedBox(height: 15),
                    Center(
                      child: Container(
                        height: 300,
                        width: 300,
                        padding: EdgeInsets.all(15),
                        child: Form(
                          key: formKey,
                          child: SingleChildScrollView(
                            child: Column(
                              children: <Widget>[
                                buildfirstname(),
                                const SizedBox(height: 20),
                                buildlastname(),
                                SizedBox(
                                  height: 25,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: ElevatedButton(
                                    child: Text("Continue",
                                        style: TextStyle(
                                            fontSize: 23,
                                            fontWeight: FontWeight.bold)),
                                    style: ButtonStyle(
                                        foregroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.black),
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.white),
                                        padding: MaterialStateProperty.all(
                                            EdgeInsets.all(10)),
                                        shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ))),
                                    onPressed: () {
                                      var isValid =
                                          formKey.currentState!.validate();
                                      if (isValid) {
                                        formKey.currentState!.save();
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    OneScreen()));
                                      }
                                      String firstName =
                                          firstnameController.text;
                                      String lastName = lastnameController.text;
                                      ref
                                          .child("Users")
                                          .push()
                                          .child('firstname')
                                          .set(firstName)
                                          .child('lastname')
                                          .set(lastName)
                                          .asStream();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    //SizedBox(height: 15),
                  ],
                ),
              )
            ])));
  }
}

class FirebaseDatabase {
  static var instance;
}
