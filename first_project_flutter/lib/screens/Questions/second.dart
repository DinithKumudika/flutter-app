import 'package:first_project_flutter/screens/Questions/third.dart';
import 'package:flutter/material.dart';

class SecondScreen extends StatefulWidget {
  @override
  _SecondScreenState createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  bool box1 = false;
  bool box2 = false;
  bool box3 = false;
  bool box4 = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
            title: Text("Internet Usage Survey"),
            backgroundColor: Colors.deepOrangeAccent),
        resizeToAvoidBottomInset: false,
        body: Container(
            color: Colors.white54,
            child: Stack(children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Positioned(
                        child: Image.asset(
                      "assets/images/1.jpg",
                      alignment: Alignment.centerRight,
                      height: size.height * 0.4,
                      width: size.width * 0.8,
                    )),
                    //SizedBox(height: 15),
                    Center(
                        child: Text('Select your age range',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 22,
                            ))),
                    //SizedBox(height: 15),
                    Container(
                      margin: const EdgeInsets.only(left: 60.0),
                      child: Column(
                        children: [
                          Row(
                            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Checkbox(
                                  activeColor: Colors.orangeAccent,
                                  value: box1,
                                  onChanged: (value) {
                                    setState(() {
                                      box1 = !box1;
                                    });
                                  }),
                              Text('15 - 20',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold))
                            ],
                          ),
                          Row(
                            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Checkbox(
                                  activeColor: Colors.orangeAccent,
                                  value: box2,
                                  onChanged: (value) {
                                    setState(() {
                                      box2 = !box2;
                                    });
                                  }),
                              Text('21 - 25',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold))
                            ],
                          ),
                          Row(
                            //mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Checkbox(
                                  activeColor: Colors.orangeAccent,
                                  value: box3,
                                  onChanged: (value) {
                                    setState(() {
                                      box3 = !box3;
                                    });
                                  }),
                              Text('26 - 30',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold))
                            ],
                          ),
                          Row(
                            //mainAxisAlignment: MainAxisAlignment.,
                            children: [
                              Checkbox(
                                  activeColor: Colors.orangeAccent,
                                  value: box4,
                                  onChanged: (value) {
                                    setState(() {
                                      box4 = !box4;
                                    });
                                  }),
                              Text('30 above',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold))
                            ],
                          ),
                        ],
                      ),
                    ),

                    //SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ThirdScreen()));
                          //Navigator.push(context, MaterialPageRoute(builder: (context) => OneScreen()),
                        },
                        //Navigator.of(context).pushNamed('OneScreen');

                        //Navigator.push(context, MaterialPageRoute(builder: context) => OneScreen());},
                        //Navigator.of(context).pushNamed('/OneScreen');

                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        color: Colors.orangeAccent,
                        padding: EdgeInsets.all(15),
                        child: Text("NEXT",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)),
                      ),
                    )
                  ],
                ),
              )
            ])));
  }
}
