import 'package:first_project_flutter/screens/Questions/fifth.dart';
import 'package:flutter/material.dart';

class FourthScreen extends StatefulWidget {
  @override
  _FourthScreenState createState() => _FourthScreenState();
}

class _FourthScreenState extends State<FourthScreen> {
  bool box1 = false;
  bool box2 = false;
  bool box3 = false;
  bool box4 = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
            title: Text("Internet Usage Survey"),
            backgroundColor: Colors.deepOrangeAccent),
        resizeToAvoidBottomInset: false,
        body: Container(
            color: Colors.white54,
            child: Stack(children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Positioned(
                        child: Image.asset(
                      "assets/images/7.png",
                      alignment: Alignment.center,
                      height: size.height * 0.4,
                      width: size.width * 0.8,
                    )),
                    //SizedBox(height: 15),
                    Container(
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Text(
                          'What is your daily Internet Usage? (Approximately)',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 19)),
                    ),

                    //SizedBox(height: 15),
                    Container(
                      margin: const EdgeInsets.only(left: 60.0),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Checkbox(
                                  activeColor: Colors.orangeAccent,
                                  value: box1,
                                  onChanged: (value) {
                                    setState(() {
                                      box1 = !box1;
                                    });
                                  }),
                              Text('Below 500MB',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold))
                            ],
                          ),
                          Row(
                            children: [
                              Checkbox(
                                  activeColor: Colors.orangeAccent,
                                  value: box2,
                                  onChanged: (value) {
                                    setState(() {
                                      box2 = !box2;
                                    });
                                  }),
                              Text('500MB - 1GB',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold))
                            ],
                          ),
                          Row(
                            children: [
                              Checkbox(
                                  activeColor: Colors.orangeAccent,
                                  value: box3,
                                  onChanged: (value) {
                                    setState(() {
                                      box3 = !box3;
                                    });
                                  }),
                              Text('1GB - 2GB',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold))
                            ],
                          ),
                          Row(
                            children: [
                              Checkbox(
                                  activeColor: Colors.orangeAccent,
                                  value: box4,
                                  onChanged: (value) {
                                    setState(() {
                                      box4 = !box4;
                                    });
                                  }),
                              Text('Above 2GB',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold))
                            ],
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => FifthScreen()));
                          //Navigator.push(context, MaterialPageRoute(builder: (context) => OneScreen()),
                        },
                        //Navigator.of(context).pushNamed('OneScreen');

                        //Navigator.push(context, MaterialPageRoute(builder: context) => OneScreen());},
                        //Navigator.of(context).pushNamed('/OneScreen');

                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        color: Colors.orangeAccent,
                        padding: EdgeInsets.all(15),
                        child: Text("NEXT",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)),
                      ),
                    )
                  ],
                ),
              )
            ])));
  }
}
