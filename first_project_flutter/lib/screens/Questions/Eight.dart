import 'package:first_project_flutter/screens/Questions/final.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/link.dart';
import 'package:url_launcher/url_launcher.dart';

class EightScreen extends StatefulWidget {
  @override
  _EightScreenState createState() => _EightScreenState();
}

//state of the widget
class _EightScreenState extends State<EightScreen> {
  final GlobalKey<FormState> formKey = GlobalKey();
  String monthlyUsage = '';

  Widget buildmonthlyusage() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Monthly Data Usage (GB)',
          labelStyle: TextStyle(
            color: Colors.black,
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.black),
            borderRadius: BorderRadius.circular(05),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.black),
            borderRadius: BorderRadius.circular(05),
          ),
          fillColor: Colors.white,
        ),
        validator: (value) {
          if (value!.length < 1) {
            return 'Monthly Usage Required';
          } else {
            return null;
          }
        },
        onSaved: (value) => setState(() => monthlyUsage = value!),
        keyboardType: TextInputType.number,
      );
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
            title: Text("Internet Usage Survey"),
            backgroundColor: Colors.deepOrangeAccent),
        resizeToAvoidBottomInset: false,
        body: Container(
            color: Colors.white54,
            child: Stack(children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Positioned(
                        child: Image.asset(
                      "assets/images/meter.png",
                      alignment: Alignment.center,
                      height: size.height * 0.3,
                      width: size.width * 0.5,
                    )),
                    //SizedBox(height: 15),
                    Container(
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Text(
                          'Go to the below Website and find out your monthly data usage. Then enter it here',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 32)),
                    ),
                    SizedBox(height: 10),
                    Link(
                      uri: Uri.parse(
                          "https://www.glasswire.com/data-calculator/"),
                      target: LinkTarget.blank,
                      builder: (context, gotoLink) {
                        return ElevatedButton(
                          onPressed: gotoLink,
                          child: Text("Go to Website",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold)),
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Colors.purpleAccent),
                          ),
                        );
                      },
                    ),
                    SizedBox(height: 10),
                    Center(
                      child: Container(
                        height: 300,
                        width: 300,
                        padding: EdgeInsets.all(15),
                        child: Form(
                          key: formKey,
                          child: SingleChildScrollView(
                            child: Column(
                              children: <Widget>[
                                buildmonthlyusage(),
                                const SizedBox(height: 10),
                                SizedBox(
                                  height: 30,
                                ),
                                Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: ElevatedButton(
                                      child: Text("Almost Done!",
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold)),
                                      style: ButtonStyle(
                                          foregroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.black),
                                          backgroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.orangeAccent),
                                          padding: MaterialStateProperty.all(
                                              EdgeInsets.all(10)),
                                          shape: MaterialStateProperty.all<
                                                  RoundedRectangleBorder>(
                                              RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                          ))),
                                      onPressed: () {
                                        final isValid =
                                            formKey.currentState!.validate();
                                        if (isValid) {
                                          formKey.currentState!.save();
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      FinalScreen()));
                                        }
                                      },
                                    ))
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ])));
  }
}
