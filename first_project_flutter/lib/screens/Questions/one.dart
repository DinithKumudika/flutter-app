import 'package:first_project_flutter/screens/Questions/second.dart';
import 'package:flutter/material.dart';

//widget
class OneScreen extends StatefulWidget {
  @override
  _OneScreenState createState() => _OneScreenState();
}

//state of the widget
class _OneScreenState extends State<OneScreen> {
  bool new1 = false;
  bool new2 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
            title: Text("Internet Usage Survey"),
            backgroundColor: Colors.deepOrangeAccent),
        body: Container(
            color: Colors.white54,
            child: Stack(children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Positioned(
                        child: Image.asset(
                      "assets/images/4.jpg",
                      alignment: Alignment.centerLeft,
                      height: size.height * 0.5,
                      width: size.width * 0.9,
                    )),
                    //SizedBox(height: 15),
                    Center(
                        child: Text('Do you use internet?',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 22,
                            ))),
                    SizedBox(height: 15),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Checkbox(
                            activeColor: Colors.orangeAccent,
                            value: new1,
                            onChanged: (value) {
                              setState(() {
                                new1 = !new1;
                              });
                            }),
                        Text('Yes',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Checkbox(
                            activeColor: Colors.orangeAccent,
                            value: new2,
                            onChanged: (value) {
                              setState(() {
                                new2 = !new2;
                              });
                            }),
                        Text('No',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold))
                      ],
                    ),

                    //SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.all(30),
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SecondScreen()));
                          //Navigator.push(context, MaterialPageRoute(builder: (context) => OneScreen()),
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        color: Colors.orangeAccent,
                        padding: EdgeInsets.all(15),
                        child: Text(" NEXT ",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)),
                      ),
                    )
                  ],
                ),
              )
            ])));
  }
}
